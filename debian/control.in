Source: gnome-shell-extension-appindicator
Section: gnome
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Matteo F. Vescovi <mfv@debian.org>, Marco Trevisan <marco@ubuntu.com>, @GNOME_TEAM@
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-gnome,
 jq,
 libglib2.0-bin,
 meson,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://github.com/ubuntu/gnome-shell-extension-appindicator
Vcs-Browser: https://salsa.debian.org/gnome-team/shell-extensions/gnome-shell-extension-appindicator
Vcs-Git: https://salsa.debian.org/gnome-team/shell-extensions/gnome-shell-extension-appindicator.git

Package: gnome-shell-extension-appindicator
Architecture: all
Depends:
 gnome-shell (>= 45~),
 gnome-shell (<< 46~),
 ${shlibs:Depends},
 ${misc:Depends}
Suggests:
 gnome-shell-extension-prefs,
 libappindicator3-1,
 libayatana-appindicator3-1,
Description: AppIndicator, KStatusNotifierItem and tray support for GNOME Shell
 This extension integrates Ubuntu AppIndicators and KStatusNotifierItems
 (KDE's blessed successor of the systray) and legacy tray icons into
 GNOME Shell.
 .
 Features:
 .
  - show indicator icons in the panel
  - reveal indicator menus upon click
  - double clicking an icon will activate the application window
    (if implemented by the indicator)
  - middle mouse click an icon to send a 'SecondaryActivate' event
    to the application. Support needs to be implemented in the application
  - supports legacy tray icons
  - icons can be themed, resized and effects can be applied
